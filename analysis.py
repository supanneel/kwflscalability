# -*- coding: utf-8 -*-
"""
Created on Tue Nov 30 10:51:56 2021
Analyse of the scalability of diffraction-limited fiber lasers and amplifier
Reference:  [1] Dawson, J.W. et al., 2008. Analysis of the scalability of 
            diffraction-limited fiber lasers and amplifiers to high average power.
            Optics Express, 16(17), p.13240.
            [2] Michalis N. Zervas, "Transverse mode instability, thermal lensing 
            and power scaling in Yb3+-doped high-power fiber amplifiers," 
            Opt. Express 27, 19019-19041 (2019)
@author: lea80558
"""

import pandas as pd
import numpy as np
import plotly.io as pio
pio.renderers.default = 'browser'
import plotly.express as px
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import matplotlib.pyplot as plt


data = 'param.xlsx'

param = pd.read_excel(data, index_col = 0)
# core radius
# a =  25e-6 
d = np.linspace(5e-6,160e-6,500)
a = d/2
# fiber length
L1 = np.linspace(0.1,100,500)
L2 = np.linspace(0.1,10,500)
# Cladding numerical aperture
NA = 0.48
# Signal wavelength
lam = 1.08e-6 

d3 = np.linspace(15e-6,150e-6,500)
a3 = d3/2
L3 = np.linspace(0.1,60,500)

def analysis(a,L,NA, lam): 
    # SRS
    Aeff = param.Value.gamma**2 * np.pi * a**2
    g = np.log(param.Value.G)/L
    Leff = 1/g*(np.exp(g*L)-1)
    
    Psrs = 16*np.pi*a**2*param.Value.gamma**2*np.log(param.Value.G)/param.Value.gR/L
     
    # SBS    
    Psbs = 17*np.pi*a**2*param.Value.gamma**2*np.log(param.Value.G)/param.Value.gB/L
    
    # Damage limitaion
    Pdmg = param.Value.gamma**2*param.Value.Idmg*np.pi*a**2
    
    # Pump power limitation
    # radius of pump cladding
    b = a*np.sqrt(param.Value.alpha_core*L/param.Value.A)  
    Ppump = param.Value.eta_laser*param.Value.Ipump*(np.pi*b**2)*(np.pi*NA**2)
    
    
    # Thermal lens
    Plens = (param.Value.eta_laser/param.Value.eta_heat)*np.pi*param.Value.k*lam**2*L/2/param.Value.dndT/a**2
    
    # Transverse mode instability
    LP01 = 2.405
    LP11 = 3.832
    V = 3
    Us = LP01*np.exp(-1/V)
    Ue = LP11*np.exp(-1/V)
    neff = 1.45 
    Ptmilens = (Ue**2)*(Ue**2 - Us**2)/(4*(np.pi**2)*neff)
    Ptmi = Ptmilens*Plens
    return Psrs, Psbs, Ppump, Ptmi

# Pre-allocate arrays
Psrs = np.empty([len(L1),len(a)])
Psbs = np.empty([len(L2),len(a)])
Ppump1 = np.empty([len(L1),len(a)])
Plens1 = np.empty([len(L1),len(a)])
Ppump2 = np.empty([len(L2),len(a)])
Plens2 = np.empty([len(L2),len(a)])
Plens3 = np.empty([len(L3),len(a3)])

# Obtain thershold powers
for idx in range(len(a)):
    Psrs[:,idx], _, Ppump1[:,idx], Plens1[:,idx] = analysis(a[idx],L1,NA,lam)
    _, Psbs[:,idx], Ppump2[:,idx], Plens2[:,idx] = analysis(a[idx],L2,NA,lam)
    # _, P_, _, Plens3[:,idx] = analysis(a[idx],L2,NA,lam)

Ppumpmax = param.Value.eta_laser*param.Value.Ipump*(np.pi*300e-6**2)*(np.pi*NA**2)
print("Maximum power with mechanical constraint =", Ppumpmax, "W")

# Get minimum thershold powers
P_sbs = np.logical_and(Psbs<Ppump2,Psbs<Plens2)*1
P_lens1 = np.logical_and(Plens2<Ppump2, Plens2<Psbs)*2

##########################SBS PLOT#############################################
# Pmin = np.minimum(Ppump2,Psbs)
# Pmin = np.minimum(Pmin,Plens2)

# fig2 = make_subplots(specs=[[{"secondary_y": True}]])
# fig2.add_trace(go.Heatmap(
#                     z=P_lens1+P_sbs,
#                     x=d,
#                     y=L2))
# fig2.add_trace(go.Contour(
#                     z=Pmin,
#                     x=d,
#                     y=L2,
#                     colorscale='Blugrn',
#                     contours=dict(
#                     coloring ='lines',
#                     showlabels = True, # show labels on contours
#                     labelfont = dict( # label font properties
#                         size = 24,
#                         color = 'white',
#                         ))
#                     ),secondary_y=True)
# fig2.add_annotation(x=30e-6, y=2.3,
#             text="SBS",
#             showarrow=False,
#             font=dict(
#                 size=30,
#             ))
# fig2.add_annotation(x=123e-6, y=0.7,
#             text="TMI",
#             showarrow=False,
#             font=dict(
#                 size=30,
#             ))
# fig2.add_annotation(x=26e-6, y=0.5,
#             text="Pump",
#             showarrow=False,
#             font=dict(
#                 size=30,
#             ))
# fig2.update_layout(font=dict(
#         size=32,
#         color="RebeccaPurple"
#     ))
# # Update xaxis properties
# fig2.update_xaxes(title_text="Core diameter (m)")
# # # Update yaxis properties
# fig2.update_yaxes(title_text="Amplifier Length (m)")
# fig2.show()

# Get minimum thershold powers
P_srs = np.logical_and(Psrs<Ppump1,Psrs<Plens1)*1
P_lens0 = np.logical_and(Plens1<Ppump1, Plens1<Psrs)*2

##########################SRS PLOT#############################################
Pmin = np.minimum(Ppump1,Psrs)
Pmin = np.minimum(Pmin,Plens1)

fig5 = make_subplots(specs=[[{"secondary_y": True}]])
fig5.add_trace(go.Heatmap(
                    z=P_lens0+P_srs,
                    x=d,
                    y=L1))
fig5.add_trace(go.Contour(
                    z=Pmin,
                    x=d,
                    y=L1,
                    colorscale='Blugrn',
                    contours=dict(
                    start=0,
                    end=Ppumpmax,
                    coloring ='lines',
                    showlabels = True, # show labels on contours
                    labelfont = dict( # label font properties
                        size = 20,
                        color = 'white',))
                    ),secondary_y=True)
fig5.add_annotation(x=30e-6, y=60,
            text="SRS",
            showarrow=False,
            font=dict(
                size=20,
            ))
fig5.add_annotation(x=123e-6, y=15,
            text="TMI",
            showarrow=False,
            font=dict(
                size=20,
            ))
fig5.add_annotation(x=26e-6, y=9.6,
            text="Pump limited",
            showarrow=False,
            font=dict(
                size=20,
            ))
fig5.update_layout(font=dict(
        size=20,
        color="RebeccaPurple"
    ))
# Update xaxis properties
fig5.update_xaxes(title_text="Core diameter (m)")
# # Update yaxis properties
fig5.update_yaxes(title_text="Amplifier Length (m)")
fig5.show()

# fig = make_subplots(specs=[[{"secondary_y": True}]])
# fig.add_trace(go.Heatmap(
#                     z=Plens1,
#                     x=d,
#                     y=L1))
# fig.add_trace(go.Contour(
#                     z=Plens1,
#                     x=d,
#                     y=L1,
#                     colorscale='Blugrn',
#                     contours=dict(
#                     coloring ='lines',
#                     showlabels = True, # show labels on contours
#                     labelfont = dict( # label font properties
#                         size = 24,
#                         color = 'white',))
#                     ),secondary_y=True)
# # Update xaxis properties
# fig.update_xaxes(title_text="Core diameter (m)")
# # # Update yaxis properties
# fig.update_yaxes(title_text="Fiber Length (m)")
# fig.show()