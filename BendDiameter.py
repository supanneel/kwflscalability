# -*- coding: utf-8 -*-
"""
Created on Wed Jan 26 11:36:07 2022
Reference:  [1] Michalis N. Zervas, "Transverse mode instability, thermal lensing 
            and power scaling in Yb3+-doped high-power fiber amplifiers," 
            Opt. Express 27, 19019-19041 (2019)
@author: lea80558
"""
import numpy as np
import plotly.io as pio
pio.renderers.default = 'browser'
import plotly.express as px
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

pts = int(1e3)
# Bend diameter(m)
Dbend = np.linspace(0.1,2.0,pts)
# Cladding diameter(m)
dclad = np.arange(100,800,100)*1e-6
# Array initialized
FITx = np.empty([len(Dbend),len(dclad)])
# Constants
Np = 0.1
m = 2.42
L = 10
n = 20
n_p = 24
ep = 0.0015
tp = 1
ts = 20*31.536e6
gamma = 1000.0

for idx in range(len(dclad)):
    # gamma = (Bp/E**2)/(B/E**2)**beta
    alpha = m/(n_p-2)
    e = 0.83*dclad[idx]/Dbend
    beta = (n_p-2)/(n-2)
    FITx[:,idx] = alpha*gamma*Np*L*((e**n*ts)**beta)/ep**n_p/tp
    
# reference
ref = np.linspace(1,1,pts)
# Create traces
fig = go.Figure()
fig.add_trace(go.Scatter(x=Dbend, y=ref,
                    line=dict(dash='dash'),name='ref'))
for idx in range(len(dclad)):
    name = str(100*(idx+1)) + 'um'
    fig.add_trace(go.Scatter(x=Dbend, y=FITx[:,idx],
                        mode='lines',name=name))

# Edit the layout
fig.update_layout(title='Failure-in-Time of optical fibers',
                   xaxis_title='Bend diameter(m)',
                   yaxis_title='log(FIT)')

fig.update_yaxes(type="log")
fig.show()